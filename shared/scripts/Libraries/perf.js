function Perf()
{
    this.arrStepDetails = new Array();
    
    function stepDetails(StepName, StartTime, EndTime, TotalTime)
    {
        this.StepName = StepName,
        this.StartTime = StartTime,
        this.EndTime = EndTime,
        this.TotalTime = TotalTime;
    }
    
    this.startPerf = function startPerf(sStepName)
    {
        var dtCurrentTime = new Date().getTime();
     
        var CurrentStep = new stepDetails(sStepName, dtCurrentTime, "", "");
        
        test.log("Started " + CurrentStep.StepName);
        
        this.arrStepDetails.push(CurrentStep);
    }
    
    this.stopPerf = function stopPerf(sStepName)
    {
        var dtCurrentTime = new Date().getTime();
        
        logCompletedStepDetails(this.arrStepDetails, sStepName, dtCurrentTime);
    }
    
    function logCompletedStepDetails(arrAllSteps, sStepName, sStepEndTime)
    {
        var s;
        var bStepFound;
        
        for(s in arrAllSteps)
        {
            bStepFound = false;
            
            if(arrAllSteps[s].StepName == sStepName)
            {
                arrAllSteps[s].EndTime = sStepEndTime;
                var dtDiff = getTimeDifference(arrAllSteps[s].EndTime,arrAllSteps[s].StartTime); //in ms
                arrAllSteps[s].TotalTime = dtDiff + "";
                //test.log("End of transaction step: " + 
                    sStepName,"Step Details: " + 
                    //"\n" + arrAllSteps[s].StepName +
                    //"\nStart Time: " + arrAllSteps[s].StartTime + 
                    //"\nEnd Time: " + arrAllSteps[s].EndTime + 
                    "Total Time Taken:" + arrAllSteps[s].TotalTime;
                arrAllSteps.pop(arrAllSteps[s]);
                bStepFound = true;
                break;
            }
        }
        
        if(!bStepFound) { test.fail("Performance Step: " + sStepName + " not found!"); }
        
        return arrAllSteps;
    }
    
    
    function getMin(sDiffInSeconds)
    {
        if(sDiffInSeconds >= 60)
        {
            return sDiffInSeconds/60;
        }
        else return 0;
        
    }
    
    function getTimeDifference(EndTime,StartTime)
    {
        var dtDiff = EndTime - StartTime;
        var dtInSeconds = "\"" + dtDiff/1000 + "\"";
            
        return dtInSeconds + "(secs)";  
    }
}