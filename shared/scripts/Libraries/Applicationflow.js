source(findFile("scripts", "Libraries/Global.js"))
source(findFile("scripts", "Libraries/Constants.js"))
source(findFile("scripts", "Libraries/Perf.js"))

function Application()
{
    
    var objGlobal = new GlobalVariables();
    var objConstants = new Constants();
    var objPerf = new Perf();
    var productQuantityGlobal;
   
    this.start = function start(sAutName)
    {
        objPerf.startPerf("Transaction Name: lunching the web site");
        
        startApplication(sAutName);
        
        objPerf.stopPerf("Transaction Name: Launch the web site");
    }
    
    //Login Home Page 
    function updateLoginInfo(ConsumerData)
    {
      // var dataset = testData.dataset("LoginDetails.tsv");
        var dataset = testData.dataset(ConsumerData);
        
        for (var record in dataset) 
        {
            objGlobal.LoginUserName = testData.field(dataset[record], "LoginUserName");
            objGlobal.LoginPassword = testData.field(dataset[record], "LoginPassword");
           
        }
    }
    
    
    this.amountCarddetails= function amountCarddetails(Card_Type,Nameoncard,CardNumber,Expiration_month,Expiration_year,Security_Code)
    {
        selectOption(waitForObject(":Checkout Step 2.cardtype_select-one"), Card_Type);
        setFocus(waitForObject(":Checkout Step 2.nameoncard_text"));
        activateBrowserTab(waitForObject(":Checkout Step 2_BrowserTab"));
        setText(waitForObject(":Checkout Step 2.nameoncard_text"), Nameoncard);
        setFocus(waitForObject(":Checkout Step 2.ccnumber_text"));
        setText(waitForObject(":Checkout Step 2.ccnumber_text"), CardNumber);
        selectOption(waitForObject(":Checkout Step 2.expmon_select-one"), Expiration_month);
        selectOption(waitForObject(":Checkout Step 2.expyear_select-one"), Expiration_year);
        setFocus(waitForObject(":Checkout Step 2.vcode_text"));
        setText(waitForObject(":Checkout Step 2.vcode_text"), Security_Code);
        clickButton(waitForObject(":Checkout Step 2.Submit_submit"));
        clickButton(waitForObject(":Finalize Your Order.Submit_submit"));
    }
    this.LoginCustomer = function LoginCustomer(sFilterCriteria, sFilterValue)
    {
        snooze(3);
        activateBrowserTab(waitForObject(":Welcome to Nordic Naturals_BrowserTab"));
        if (object.exists(":Welcome to Nordic Naturals.logoutim_A"))
            
        {
            //waitForObject(":Add To Cart.logoutim_IMG");
            clickButton(waitForObject(":Welcome to Nordic Naturals.logoutim_A"));  
           //test.log("entered into if");
            /*var button = findObject(":Add To Cart.logoutim_IMG");
            clickButton(button);
            snooze(3);*/
            //updateLoginInfo(Profesinaldata);
           // clickButton(waitForObject(":Welcome to Nordic Naturals.loginbut_IMG"));
            activateBrowserTab(waitForObject(":My Nordic Log In_BrowserTab"));
            setFocus(waitForObject(":My Nordic Log In.emailadd_text"));
            setText(waitForObject(":My Nordic Log In.emailadd_text"), sFilterCriteria);
            setText(waitForObject(":My Nordic Log In.password_password"), sFilterValue);
            clickButton(waitForObject(":My Nordic Log In.Submit_submit"));
        }
    
 else{  
     //updateLoginInfo(Profesinaldata);
     clickButton(waitForObject(":Welcome to Nordic Naturals.loginbut_IMG"));
     activateBrowserTab(waitForObject(":My Nordic Log In_BrowserTab"));
     setFocus(waitForObject(":My Nordic Log In.emailadd_text"));
     setText(waitForObject(":My Nordic Log In.emailadd_text"), sFilterCriteria);
     setText(waitForObject(":My Nordic Log In.password_password"), sFilterValue);
     clickButton(waitForObject(":My Nordic Log In.Submit_submit"));
 }
      
        
 return true;
    }
    
         
    
  /*  this.LoginCustomer = function LoginCustomer(sFilterCriteria, sFilterValue)
    {
        clickButton(waitForObject(":Welcome to Nordic Naturals.loginbut_IMG"));
        activateBrowserTab(waitForObject(":My Nordic Log In_BrowserTab"));
        setFocus(waitForObject(":My Nordic Log In.emailadd_text"));
        setText(waitForObject(":My Nordic Log In.emailadd_text"), sFilterCriteria);
        setText(waitForObject(":My Nordic Log In.password_password"), sFilterValue);
        clickButton(waitForObject(":My Nordic Log In.Submit_submit"));
    }*/
    
    this.getCustomerDetailsColNo = function getCustomerDetailsColNo(sColumnName)
    {
        switch (sColumnName) 
        {
            case "LoginUserName":
                return 0;
            case "LoginPassword":
                return 1;
           
        default:
            break;
        }
        }
    
    this.getcarddetails= function getcarddeatilscolNo(sColumnName)
    {
        switch (sColumnName)
        {
        case "Card_Type":
        return 0;
    case "Nameoncard":
        return 1;
    case "CardNumber":
        return 2;
    case "Expiration_month":
        return 3;
    case"Expiration_year":
        return 4;
    case "Security_Code":
        return 5;
   
default:
    break;
        }
    }
    
  //enter Login Details & input should come from Database 
    
 /*   this.login = function loginToApplication(ConsumerData,testcasename)
   {
     
       //object.exists(":Nordic Naturals.Log out_A")
       
       //var logout = waitForObjectExists(":Welcome to Nordic Naturals.logoutim_IMG");
       
       //test.log(logout);
       if (object.exists(":Nordic Naturals.Log out_A"))
           
       {
           test.log("verified");
           waitForObject(":Nordic Naturals.Log out_A");
           
          //test.log("entered into if");
           var button = findObject(":Nordic Naturals.Log out_A");
           clickButton(button);
           snooze(3);
           updateLoginInfo(Profesinaldata);
           clickLink(waitForObject(":Nordic Naturals.Log in_A")); 
           objPerf.startPerf("Transaction Name: Login to Application");
           test.log("data entered");
           setFocus(waitForObject(":txtLoginUserName"));
           setText(waitForObject(":txtLoginUserName"),objGlobal.LoginUserName);
           test.log("username entered");
           setFocus(waitForObject(":User account | Nordic Naturals.pass_password"));
           setText(waitForObject(":User account | Nordic Naturals.pass_password"),objGlobal.LoginPassword);
           clickButton(waitForObject(":User account | Nordic Naturals.Log in_submit"));
            objPerf.stopPerf("Transaction Name: Login to Application");
       
       }
       
       else {
      
       clickLink(waitForObject(":Welcome to Nordic Naturals.loginbut_A"));
       activateBrowserTab(waitForObject(":My Nordic Log In_BrowserTab"));
       
       setFocus(waitForObject(":txtLoginUserName"));
       setText(waitForObject(":txtLoginUserName"),objGlobal.LoginUserName);
       test.log("username entered");
       
      // setFocus(waitForObject(":My Nordic Log In.emailadd_text"));
     //setText(waitForObject(":My Nordic Log In.emailadd_text"), "manasa.1101@gmail.com");
       
       setFocus(waitForObject(":User account | Nordic Naturals.pass_password"));
       setText(waitForObject(":User account | Nordic Naturals.pass_password"),objGlobal.LoginPassword);
                    
       //setText(waitForObject(":My Nordic Log In.password_password"), "manasa11");
       clickButton(waitForObject(":My Nordic Log In.Submit_submit"));
        
   }

   clickLink(waitForObject(":Nordic Naturals Products_A"));
   clickLink(waitForObject(":Product Details.adder_A"));
   clickLink(waitForObject(":Product Details_A"));
   setFocus(waitForObject(":Add To Cart.drumcart_Quantity_0_text"));
   setText(waitForObject(":Add To Cart.drumcart_Quantity_0_text"), "3");
   clickButton(waitForObject(":Add To Cart.drumcart_Update_1_image"));
   clickButton(waitForObject(":Add To Cart.drumcart_Checkout_1_image"));
   selectOption(waitForObject(":Checkout Step 2.cardtype_select-one"), "MasterCard");
   clickButton(waitForObject(":Checkout Step 2.Submit_submit"));
  } */
    
   
   
   this.selectingproduct=function selectingproduct(ProductQuantity)
   {
       clickLink(waitForObject(":Nordic Naturals Products_A"));
       clickLink(waitForObject(":Product Details.adder_A"));
       clickLink(waitForObject(":Product Details_A"));
        //setFocus(waitForObject(":Add To Cart.drumcart_Quantity_0_text"));
        //setText(waitForObject(":Add To Cart.drumcart_Quantity_0_text"), "2");
       // clickButton(waitForObject(":Add To Cart.drumcart_Update_1_submit"));
        //clickButton(waitForObject(":Add To Cart.drumcart_Checkout_1_submit"));
        

        snooze(5);
        //test.vp("VP1");
       
       //findItemByText(text); 
       //findObject(object)
      //  typeText("Add To Cart.drumcart_Quantity_0_text", 2);
        //clickItem(waitForObject("{container=':Add To Cart_BrowserTab' form='form1' name='drumcart_Quantity_0' tagName='INPUT' type='text' visible='true'}"));
        //setText(waitForObject(":Add To Cart.drumcart_Quantity_0_text"), ProductQuantity);
       // clickButton(waitForObject(":Add To Cart.drumcart_Update_1_submit"));
       // clickButton(waitForObject(":Add To Cart.drumcart_Checkout_1_submit"));
        //llclickLink(waitForObject(":Checkout Step 2.logoutim_A"));
       
       //setFocus(waitForObject(":Add To Cart.drumcart_Quantity_0_text"));
       //setText(waitForObject(":Add To Cart.drumcart_Quantity_0_text"), "3");
       //clickButton(waitForObject(":Add To Cart.drumcart_Update_1_image"));
       //clickButton(waitForObject(":Add To Cart.drumcart_Checkout_1_image"));
      // selectOption(waitForObject(":Checkout Step 2.cardtype_select-one"), "MasterCard");
     //  clickButton(waitForObject(":Checkout Step 2.Submit_submit"));
            
   }
   this.logotAccount = function logoutAccount(){
       objPerf.startPerf("TractionName:Logout user Account ");
       clickButton(waitForObject(":Add To Cart.logoutim_IMG"));
       objPerf.stopPerf("TractionName:Logout user Account");
       return true;
   }
   this.getOrderDetailsColNo = function getOrderDetailsColNo(sColumnName)
   {
       switch (sColumnName) 
       {
           case"ProductName":
               return 0;
           case "ProductQuantity":
               return 1;
          
       default:
           break;
       }
   }
   
 
   
  
}