source(findFile("scripts", "Libraries/Global.js"))
source(findFile("scripts", "Libraries/Constants.js"))
source(findFile("scripts", "Libraries/Perf.js"))

function Application()
{
    
    var objGlobal = new GlobalVariables();
    var objConstants = new Constants();
    var objPerf = new Perf();
    var productQuantityGlobal;
    //var Global = new GlobalVariables();
    
    //To Start The XTuple Application 
    this.start = function start(sAutName)
    {
        objPerf.startPerf("Transaction Name: Launch xTuple Application");
        
        startApplication(sAutName);
        
        objPerf.stopPerf("Transaction Name: Launch xTuple Application");
    }
    
    //Login Home Page 
    function updateLoginInfo(Profesinaldata)
    {
      // var dataset = testData.dataset("LoginDetails.tsv");
        var dataset = testData.dataset(Profesinaldata);
        
        for (var record in dataset) 
        {
            objGlobal.LoginUserName = testData.field(dataset[record], "LoginUserName");
            objGlobal.LoginPassword = testData.field(dataset[record], "LoginPassword");
           
        }
    }
    
    this.searchAndSelectCustomer = function searchAndSelectCustomer(sFilterCriteria, sFilterValue)
    {
    
    }
  //enter Login Details & input should come from Database 
    
    this.login = function loginToApplication(Profesinaldata)
   {
       activateBrowserTab(waitForObject(":Nordic Naturals_BrowserTab"));
      //waitForObject(":Nordic Naturals.Log out_A");
       snooze(3);
       //findObject(":Nordic Naturals.Log out_A");
      //test.log("object exists");
       if (object.exists(":Nordic Naturals.Log out_A"))
           
       {
           waitForObject(":Nordic Naturals.Log out_A");
           
          //test.log("entered into if");
           var button = findObject(":Nordic Naturals.Log out_A");
           clickButton(button);
           snooze(3);
           updateLoginInfo(Profesinaldata);
           clickLink(waitForObject(":Nordic Naturals.Log in_A")); 
           objPerf.startPerf("Transaction Name: Login to Application");
           test.log("data entered");
           setFocus(waitForObject(":txtLoginUserName"));
           setText(waitForObject(":txtLoginUserName"),objGlobal.LoginUserName);
           test.log("username entered");
           setFocus(waitForObject(":User account | Nordic Naturals.pass_password"));
           setText(waitForObject(":User account | Nordic Naturals.pass_password"),objGlobal.LoginPassword);
           clickButton(waitForObject(":User account | Nordic Naturals.Log in_submit"));
            objPerf.stopPerf("Transaction Name: Login to Application");
       }
   
else
{  
 updateLoginInfo(Profesinaldata);
 clickLink(waitForObject(":Nordic Naturals.Log in_A")); 
    objPerf.startPerf("Transaction Name: Login to Application");
    test.log("data entered");
    setFocus(waitForObject(":txtLoginUserName"));
    setText(waitForObject(":txtLoginUserName"),objGlobal.LoginUserName);
    test.log("username entered");
    setFocus(waitForObject(":User account | Nordic Naturals.pass_password"));
    setText(waitForObject(":User account | Nordic Naturals.pass_password"),objGlobal.LoginPassword);
    clickButton(waitForObject(":User account | Nordic Naturals.Log in_submit"));
     objPerf.stopPerf("Transaction Name: Login to Application");
    /*objPerf.startPerf("Transaction Name: Logout");
    activateBrowserTab(waitForObject(":Nordic Naturals_BrowserTab"));
    clickLink(waitForObject(":Nordic Naturals.Log out_A"));
    objPerf.stopPerf("Transaction Name: Logout");*/
    

}
 return true;
   }  
    
   
   
   this.selectingproduct=function selectingproduct()
   {
       setFocus(waitForObject(":Nordic Naturals.query_text"));
       setText(waitForObject(":Nordic Naturals.query_text"), "485");
       clickButton(waitForObject(":Nordic Naturals.search_submit"));
       setText(waitForObject(":Connecting….query_text"), "<Return>");
       clickLink(waitForObject(":Search | DHA Infant™"));
       
        setFocus(waitForObject(":DHA Infant™ | Nordic Naturals.quantity_number"));
        setText(waitForObject(":DHA Infant™ | Nordic Naturals.quantity_number"), "3");
      
        clickButton(waitForObject(":Arctic-D Cod Liver Oil™ | Nordic Naturals.Add to cart_submit"));
        clickLink(waitForObject("DHA Infant™| Nordic Naturals.(0)_A"));
 
      
       
   }
  
   
}